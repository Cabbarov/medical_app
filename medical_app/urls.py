from django.urls import path
from .views import *

urlpatterns = (
    path( '', medical_view, name = 'medical' ),
    path( 'medical/<int:id>/category', category_view, name = 'category' ),
    path( 'category/<int:id>/doctor', doctor_view, name = 'doctor' ),
    path( 'doctor/<int:id>/appointment', create_reserve, name = 'appointment' ),
)