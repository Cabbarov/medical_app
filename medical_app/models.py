from django.db import models


class Medicallist( models.Model ):
    medical_name = models.CharField( max_length = 100 )

    def __str__(self):
        return f"{self.medical_name}"


class Category( models.Model ):
    med_id = models.ForeignKey( Medicallist, on_delete = models.CASCADE, null = True, blank = True )
    category_name = models.CharField( max_length = 100, null=True, blank=True )

    def __str__(self):
        return f"{self.med_id}, {self.category_name}"


class Doctor( models.Model ):
    category_id = models.ForeignKey( Category, on_delete = models.CASCADE, null = True, blank = True )
    name = models.CharField( max_length = 50 )
    surname = models.CharField( max_length = 50 )
    age = models.IntegerField( default = None )
    carrier = models.CharField( max_length = 1000 )
    picture = models.ImageField( upload_to = "post/",
                                 null = True, blank = True )

    def __str__(self):
        return f"{self.name}, {self.surname}, {self.age}, {self.carrier}, {self.picture}"

    """@property
    def short_name(self):
        return '{} {}.'.format( self.surname.title(), self.name[ 0 ].upper() )"""


class Reserve( models.Model ):
    unique_together = ('doctor', 'date', 'timeslot')

    TIMESLOT_LIST = (
        (0, '09:00 – 10:00'),
        (1, '10:00 – 11:00'),
        (2, '11:00 – 12:00'),
        (3, '12:00 – 13:00'),
        (4, '13:00 – 14:00'),
        (5, '14:00 – 15:00'),
        (6, '15:00 – 16:00'),
        (7, '16:00 – 17:00'),
        (8, '17:00 – 18:00'),
    )

    doctor = models.ForeignKey( Doctor, on_delete = models.CASCADE )
    name = models.CharField( max_length = 50 )
    surname = models.CharField( max_length = 50 )
    age = models.IntegerField( default = None )
    date = models.DateField()
    timeslot = models.IntegerField( choices = TIMESLOT_LIST )
    contact_number = models.CharField( max_length = 15 )
    comment = models.CharField( max_length = 1000 )

    def __str__(self):
        return f"{self.name}, {self.surname}, {self.age}, {self.date}, {self.timeslot}, {self.comment}"

    """#@property
    def time(self):
        return self.TIMESLOT_LIST[ self.timeslot ][ 1 ]"""
