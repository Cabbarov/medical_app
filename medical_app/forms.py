from datetime import date

from django import forms
from datetimewidget.widgets import DateTimeWidget
from .models import *


class MedicalForm(forms.ModelForm):
    class Meta:
        model = Medicallist
        fields = ['medical_name']

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ['category_name']



class ReserveForm(forms.ModelForm):
    class Meta:
        model = Reserve
        fields = ['doctor', 'name', 'surname','comment',
                  'age', 'date', 'timeslot', 'contact_number']
        
    
        


        #widgets = {
        #     'date': DateTimeWidget(
        #         attrs={'id': 'date'}, usel10n=False, bootstrap_version=3,
        #         options={
        #             'minView': 2,  
        #             'maxView': 3,  
        #             'weekStart': 1,
        #             'todayHighlight': True,
        #             'format': 'yyyy-mm-dd',
        #             'daysOfWeekDisabled': [0, 6],
        #             'startDate': date.today().strftime('%Y-%m-%d'),
        #         }),
        # }
    
    # def clean_date(self):
    #     day = self.cleaned_data['date']

    #     if day <= date.today():
    #         raise forms.ValidationError('Date should be upcoming (tomorrow or later)', code='invalid')
    #     if day.isoweekday() in (0, 6):
    #         raise forms.ValidationError('Date should be a workday', code='invalid')

    #     return day

