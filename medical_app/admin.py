from django.contrib import admin
from .models import Medicallist, Category, Doctor, Reserve

@admin.register(Medicallist)
class MedicallistAdmin(admin.ModelAdmin):
    list_display = ('id', 'medical_name')

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'med_id', 'category_name')

@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    list_display = ('name', 'surname', 'age', 'carrier', 'picture')

@admin.register(Reserve)
class ReserveAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'surname', 'age', 'date', 'timeslot', 'comment', 'contact_number')

