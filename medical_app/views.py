from django.http import Http404
from django.shortcuts import render, get_list_or_404, redirect
from .models import  Medicallist, Category, Reserve
from .forms import *


def medical_view(request):
    context = {}
    context["medicals"] = Medicallist.objects.all()
    return render(request, 'medicallist.html', context)

def category_view(request, id):
    context = {}
    try:
        context['category'] = Category.objects.filter(med_id__pk=id)
    except Category.DoesNotExist:
        raise Http404
    return render(request, 'category_list.html', context)

def doctor_view(request, id):
    context = {}
    context['doctor'] = Doctor.objects.filter(category_id__pk=id)
    return render(request, 'doctor_list.html', context)

def create_reserve(request, id):
    context = {}
    form = ReserveForm(request.POST or None)
    date = request.POST.get("date")
    timeslot = request.POST.get("timeslot")
    if Reserve.objects.filter(date=date,timeslot=timeslot):
        return redirect("create_reserve")
    if form.is_valid():
        form.save()
        return redirect('medical_view')
    context['form'] = form
    return render(request, 'reserve.html', context)


